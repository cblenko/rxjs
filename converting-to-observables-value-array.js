// converting to observables

const Rx = require('rxjs'),
      fs = require('fs');

{
    // from a series of values / variables
    let o = Rx.Observable.of('Value One', 'Value Two');

    let [a, b] = ['One', 'Two'];
    let m = Rx.Observable.of(a, b);
}

{
    // from an array
    let o = Rx.Observable.from(['Value One', 'Value Two']);
}
