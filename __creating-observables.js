const Rx = require('rxjs');


// observables where the next method is exposed and can be called to push a new value to the subscriptions
// can be used with external events / frameworks
let ext = new Rx.Subject();

ext.subscribe( (v) => {
    console.log(v);
})

ext.next(1);
ext.next(2);
ext.next(3);


// observable where the next method is controlled from within the fn creating the observable
let int = Rx.Observable.create(observer => {
    observer.next('foo');
    setTimeout(() => observer.next('bar'), 1000);
});
int.subscribe(value => console.log(value));