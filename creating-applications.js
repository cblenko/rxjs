let button = document.querySelector('button');

Rx.Observable.fromEvent(button, 'click')
    .scan( (acc, curr) => acc + 1, 0)         // scan (reduce) to a stream of counts (curr not used here and 0 is the seed value)
    .subscribe(count =>  {
        document.querySelector('#clickCount').innerHTML = count
    });