let input = Rx.Observable.fromEvent(document.querySelector('input'), 'input')



input
    // .map( (event) => event.target.value)     // map the value to another value (input: event, output: event.target.value)
    .pluck('target', 'value')                   // strings to represent a deep property in the given event
    // .pairwise()                              // returns the current and previous value
    .subscribe( (v) => {
        console.log(v);
    })


Rx.Observable.of(1, 1, 2, 2, 2, 1, 2, 3, 4, 3, 2, 1)
    //.distinct()                                 // only passes distinct values to next operator
    .distinctUntilChanged()                       // does not allow repeating values in sequence
    .subscribe(x => console.log(x)); // 1, 2, 3, 4