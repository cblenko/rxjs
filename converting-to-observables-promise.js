// converting to observables

const Rx = require('rxjs'),
      fs = require('fs');

{
    // from a promise
    let p = new Promise( (resolve, reject) => {
        setTimeout( () => {
            resolve(1);
        }, 1);
    })

    Rx.Observable.fromPromise(p)
    .subscribe( (v) => {
        console.log(v);
    })
}
