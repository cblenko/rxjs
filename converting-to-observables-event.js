// converting to observables

const Rx = require('rxjs'),
      fs = require('fs');

if(!global){    // will not work on Node as it's a window event
    // fron an event
    let first = Rx.Observable.fromEvent(window, 'mousemove');

    var a = first.take(1)
        .subscribe( (e) => {
            console.log(e);
        })
        
    a.unsubscribe();
}
