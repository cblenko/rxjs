// converting to observables

const Rx = require('rxjs'),
      fs = require('fs');

{
    // bind to a callback method - where the last argument is the callback function
    let exists = Rx.Observable.bindCallback(fs.exists)

    exists('c:\\a.txt').subscribe( (ex) => {
        console.log(ex);
    })
}

{
    // bind to a (node) callback method - where the last argument is the callback function
    // and the response will have 2 arguments - err, response
    let exists = Rx.Observable.bindCallback(fs.readFile)

    exists('c:\\a.txt').subscribe( (err, resp) => {
        console.log(err, resp);
    })
}
