//const Rx = require('rxjs');

let input = Rx.Observable.fromEvent(document.querySelector('input'), 'input'),
    btn = Rx.Observable.fromEvent(document.querySelector('button'), 'click');

input
    // .filter( (event) => event.target.value.length > 2)           // only log values when length greater than
    // .delay(200)                                                  // delay the event
    // .throttleTime(200)                                           // only let an event through every x ms. NOTE: you could miss the final event using this
    // .debounceTime(200)                                           // debounce - every 200ms but always captures the last event
    // .take(2)                                                     // stop streaming the events after the first 2
    .takeUntil(btn)
    .map( (event) => event.target.value )                           // map to the event.target.value for the next operator
    .subscribe( (v) => {
        console.log(v);
    })
